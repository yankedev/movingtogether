import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as qs from 'qs';
import axios, { AxiosRequestConfig } from 'axios';

admin.initializeApp();

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript


// As described in https://dev.fitbit.com/build/reference/web-api/subscriptions/
// FITBIT servers POST to https://us-central1-movetogether-fll.cloudfunctions.net/addFitbitSubscriptionMessage
export const addFitbitSubscriptionMessage = functions.https.onRequest(async (req, res) => {
  // Check for POST request
  if(req.method !== "POST"){
    // fitbit servers verify the subscription API with a special GET call 
    // see https://dev.fitbit.com/build/reference/web-api/subscriptions/#verify-a-subscriber
    const verifyCode = req.query.verify;
    if (verifyCode) {
        if (verifyCode === '492d17818760969fb6b63415c019a8884d7f2b8facea0d619cec492b39d87eff') {
            res.status(204).send('VerifyCode OK'); 
            return;       
        }else{
            res.status(404).send('Wrong VerifyCode');
            return;
        }
    } 

    res.status(400).send('Please send a POST request');
    return;
  }
  // Grab the request payload.
  const data = req.body;
  functions.logger.info("Received a new message from FITBIT server (see next log message): ", {structuredData: true});
  functions.logger.info(data, {structuredData: true});

  // Push the new message into Cloud Firestore using the Firebase Admin SDK.
  const writeResult = await admin.firestore().collection('fitbit').add(data);

  // Send back a message that we've succesfully written the message
  res.status(204).json({result: `FitBit message added with ID: ${writeResult.id}.`});
});

/**
 * This is an asynchronous function that uses async-await with request-promise
 * to fetch a result from Fitbit web server.
 * @returns {Promise<string>}
 */
async function getToken(code: any, redirectUri: string): Promise<any> {

  let clientId = functions.config().stepsprovider.fitbit.id;
  let clientSecret = functions.config().stepsprovider.fitbit.key;

  let buff = Buffer.from(clientId+":"+clientSecret);
  let base64data = buff.toString('base64');

  let response = {};
  // Here we go!
  let data = {
    clientId: clientId,
    grant_type: 'authorization_code',
    redirect_uri: redirectUri,
    code: code
  }
  let options: AxiosRequestConfig =  
              {
                headers: { 
                  Authorization: `Basic ${base64data}`,
                  'content-type': 'application/x-www-form-urlencoded' }
              };

  functions.logger.info(qs.stringify(data));
  await axios.post("https://api.fitbit.com/oauth2/token", qs.stringify(data), options)
      .then((body) => response = body.data)
      .catch ((err) => { 
        functions.logger.error(err);
        response = { "err": err.toString() }; });
  
  return response;
}


export const addFitbitUser = functions.https.onRequest(async (req, res) => {
  // Grab the request payload.
  const code: any = req.query.code;
  functions.logger.info(req.query);
  functions.logger.info(`Received an Authorization code from Fitbit server: ${code}, I'm going to exchange it for a token`, {structuredData: true});
  let redirectUrl = functions.config().stepsprovider.fitbit.authorizationcode.redirecturl;
  functions.logger.info(`Using ${redirectUrl} as redirectUrl`, {structuredData: true});
    
  let token = await getToken(code, redirectUrl);

  functions.logger.info("Received the token from Fitbit server");
  functions.logger.info(token);

  // Push the new message into Cloud Firestore using the Firebase Admin SDK.
  await admin.firestore().collection('fitbit').add(token);
  // After having stored the fitbit token in firestore we redirect to the client application
  let homepageUrl = functions.config().website.homepage;
  functions.logger.info(`Using ${homepageUrl} as redirect to homepage Url`, {structuredData: true});
 
  res.redirect(301, homepageUrl);
});

