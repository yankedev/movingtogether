import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common'; 

import { environment } from '../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'move-together';
  access_token: string = '';
  user_id: string = '';
  userSteps7d: string[] = [];

  constructor(public router: Router, @Inject(DOCUMENT) readonly document: Document, public httpClient: HttpClient){}

  /** The Window object from Document defaultView */
  get window(): Window { return this.document.defaultView; }

  public linkWithFitbit(): void {
    // Authorization Code Grant Flow:
    // https://www.fitbit.com/oauth2/authorize?response_type=code&client_id=22942C&redirect_uri=https%3A%2F%2Fexample.com%2Ffitbit_auth&scope=activity%20nutrition%20heartrate%20location%20nutrition%20profile%20settings%20sleep%20social%20weight

    // Authorization Code Grant Flow with PKCE:
    // https://www.fitbit.com/oauth2/authorize?response_type=code&client_id=22942C&redirect_uri=https%3A%2F%2Fexample.com%2Ffitbit_auth&code_challenge=E9Melhoa2OwvFrEMTJguCHaoeK1t8URWbuGJSstw-cM&code_challenge_method=S256&scope=activity%20nutrition%20heartrate%20location%20nutrition%20profile%20settings%20sleep%20social%20weight

    const clientId="22C2GT";
    let fitbitLoginUrl = 'https://www.fitbit.com/oauth2/authorize?response_type=code&client_id='+clientId;
    if (environment.production) {
      fitbitLoginUrl += '&redirect_uri=https%3A%2F%2Fus-central1-movetogether-fll.cloudfunctions.net%2FaddFitbitUser'
    } else {
      fitbitLoginUrl += '&redirect_uri=http%3A%2F%2Flocalhost:5001%2Fmovetogether-fll%2Fus-central1%2FaddFitbitUser';
    }
    fitbitLoginUrl += '&scope=activity%20profile&expires_in=604800'
    this.redirect(fitbitLoginUrl, '_self');
  }

  getFitbitInfo(){
    let location = this.window.location;
    let urlParam = new URLSearchParams(location.hash.slice(1));
    this.access_token = urlParam.get('access_token');
    this.user_id = urlParam.get('user_id');
    this.userSteps7d = [];

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: 'Bearer '+this.access_token
      })
    };
    this.httpClient.get('https://api.fitbit.com/1/user/-/profile.json', httpOptions).subscribe((data) => console.log(data));

    this.httpClient.get('https://api.fitbit.com/1/user/'+this.user_id+'/activities/steps/date/today/7d.json', httpOptions)
                   .subscribe((steps7d: []) => {
                      console.log(steps7d['activities-steps']);
                      this.userSteps7d = steps7d['activities-steps'];
                    });
  }

    /** Redirects to the specified external link with the mediation of the router */
  public redirect(url: string, target = '_blank'): Promise<boolean> {

      return new Promise<boolean>( (resolve, reject) => {
        
       try { resolve(!!this.window.open(url, target)); }
       catch(e) { reject(e); }
    });
  }

}
