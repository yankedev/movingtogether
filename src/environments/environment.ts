// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDAYB23mGXovSzub0VHxdaF-eNKwktemeo',
    authDomain: 'movetogether-fll.firebaseapp.com',
    databaseURL: 'https://movetogether-fll.firebaseio.com',
    projectId: 'movetogether-fll',
    storageBucket: 'movetogether-fll.appspot.com',
    messagingSenderId: '869981967492',
    appId: '1:869981967492:web:5fecacae217f15c9cb5ed1',
    measurementId: 'G-65P8WE6FEZ'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
